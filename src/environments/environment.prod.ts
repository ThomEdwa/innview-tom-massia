export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA6L-oDINBDpSha71JRJkqPbzeky6MlSLk",
    authDomain: "innview-showcase.firebaseapp.com",
    databaseURL: "https://innview-showcase.firebaseio.com",
    projectId: "innview-showcase",
    storageBucket: "innview-showcase.appspot.com",
    messagingSenderId: "752817967769"
  },
  mailApi: '/assets/list.json'
};
