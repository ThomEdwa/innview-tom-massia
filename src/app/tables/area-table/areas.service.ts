
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs';

@Injectable()
export class AreasService {

  private basePath = '/areas';
  areasRef: AngularFireList<any>;
  areaRef: AngularFireObject<any>;

  areas: Observable<any>;
  area: Observable<any>;

  constructor(private db: AngularFireDatabase) {
    this.areasRef = this.db.list(this.basePath);
  }

  getareasList() {
    return this.areasRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getareasforproject(key: string): Observable<any> {
    const path = this.basePath;
    this.area = this.db.list(path, areasRef => areasRef.orderByChild('project').equalTo(key)).valueChanges();
    return this.area;
  }

  createarea(area) {
    this.areasRef.push(area);
  }

  updateareaMessage(key: string, value: any) {
    console.log('key',key,'value',value);
    this.areasRef.update(key, {messages: value.messages});
  }

  deletearea(key: string) {
    this.areasRef.remove(key);
  }

  deleteAll() {
    this.areasRef.remove();
  }
}
