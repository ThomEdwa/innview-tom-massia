import 'rxjs/add/operator/switchMap';
import { Component, OnInit ,Input, Inject, ElementRef, ViewChild} from '@angular/core';
import { ExampleDatabase, ExampleDataSource } from './helpers.data';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material' ;

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Project } from './data'
import { AngularFireAuth } from 'angularfire2/auth';


export interface areasElement {
	key: string;
	description: string;
  }


@Component({
  selector: 'app-area-table',
  templateUrl: './area-table.component.html',
	styleUrls: ['./area-table.component.scss']

})
export class AreaTableComponent implements OnInit {
	displayedColumns: string[] = ['phase', 'description'];
	dataSource : Observable<Array<String>>;
	ProjectsDataSource: Object;
	dataHolder : Object;

	@Input() Id: string;
	sub : any;
	projId : Observable<Project>;
	
	constructor(@Inject('AreasService') private service,
	@Inject('DataService') private dataService,
	@Inject('ProjectsService') private ProjectsService,
	private route: ActivatedRoute
	) {}
	

  	ngOnInit() {

		this.dataHolder = ['project':{},'areas' :[]];

		this.projId = this.route.snapshot.params.id
		if(this.dataHolder != undefined){

			this.ProjectsService.getproject(this.projId).subscribe(project =>
				this.dataHolder = project;
				);

			this.service.getareasforproject(this.projId).subscribe(areas =>
				this.dataHolder =  areas;
				);
		

			this.dataSource = this.dataHolder;
		}
}
