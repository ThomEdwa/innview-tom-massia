import { Component, OnInit , Inject,Input, ElementRef, ViewChild, Output} from '@angular/core';
import { ExampleDatabase, ExampleDataSource } from './helpers.data';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DataService } from './data-retrieval'
import { Project } from './data'

export interface ProjectsElement {
	name: string;
	id: string;
	client: string;
	siteStart: string;
	postCode: string;
  }
  

@Component({
  selector: 'app-fixed-table',
  templateUrl: './fixed-table.component.html',
  styleUrls: ['./fixed-table.component.scss']
})
export class FixedTableComponent implements OnInit {
	displayedColumns: string[] = ['id', 'name', 'client', 'siteStart', 'postCode'];
	dataSource = "";
	dataTag : string;


	constructor(@Inject('ProjectsService') private service,
	@Inject('DataService') private dataService
	) {}


		
  ngOnInit() {


		this.service.getprojectsList().subscribe(projects => {
		this.dataSource = projects;
		});
	}

	
}
