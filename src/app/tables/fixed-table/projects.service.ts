
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs';

@Injectable()
export class ProjectsService {

  private basePath = '/projects';
  projectsRef: AngularFireList<any>;
  projectRef: AngularFireObject<any>;

  projects: Observable<any>;
  project: Observable<any>;

  constructor(private db: AngularFireDatabase) {
    this.projectsRef = this.db.list(this.basePath);
  }

  getprojectsList() {
    return this.projectsRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getproject(key: string): Observable<any> {
    const path = `${this.basePath}/${key}`;
    this.project = this.db.object(path).valueChanges();
    return this.project;
  }

  createproject(project) {
    this.projectsRef.push(project);
  }

  updateprojectMessage(key: string, value: any) {
    console.log('key',key,'value',value);
    this.projectsRef.update(key, {messages: value.messages});
  }

  deleteproject(key: string) {
    this.projectsRef.remove(key);
  }

  deleteAll() {
    this.projectsRef.remove();
  }
}
